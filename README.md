

# Getting started

## Installation

Clone the repository

   git clone https://dabarto@bitbucket.org/dabarto/gery-halcyon-asssignment.git

up the docker

    sh run.sh

enter to database bash

    sh mariadb.sh
    mariadb -u root -p
the password is `root`

create database

    create database example;
   
exit from database bash 

    ctrl+c or exit;
   

enter the app workspace 

    sh workspace.sh

run composer install

    composer install


Copy the example env file and make the required configuration changes in the .env file.
please keep the DB_HOSST still `mariadb`

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

You can now access the server at http://localhost


preview video :
https://youtu.be/13KYwMJb6xE