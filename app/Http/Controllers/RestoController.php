<?php
  
namespace App\Http\Controllers;
  
use App\Resto;
use App\Http\Requests\RestoRequest;
use Illuminate\Http\Request;
  
class RestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resto = Resto::latest()->paginate(5);
  
        return view('resto.index',compact('resto'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resto.create');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestoRequest $request)
    {
  
        Resto::create($request->all());
   
        return redirect()->route('resto.index')
                        ->with('success','Resto created successfully.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Resto  $resto
     * @return \Illuminate\Http\Response
     */
    public function show(Resto $resto)
    {
        return view('resto.show',compact('resto'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resto  $resto
     * @return \Illuminate\Http\Response
     */
    public function edit(Resto $resto)
    {
        return view('resto.edit',compact('resto'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resto  $resto
     * @return \Illuminate\Http\Response
     */
    public function update(RestoRequest $request, Resto $resto)
    {
        $request->validate([
            'name' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
  
        $resto->update($request->all());
  
        return redirect()->route('resto.index')
                        ->with('success','Resto updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resto  $resto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resto $resto)
    {
        $resto->delete();
  
        return redirect()->route('resto.index')
                        ->with('success','Resto deleted successfully');
    }
}