<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto extends Model
{
    protected $table = 'resto';
    protected $fillable = [
        'name', 'lat', 'lng', 'address'
    ];
}
