@extends('resto.layout')
 
@section('content')
    
    <div class="row">
        <br> &nbsp;
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Map</h2>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div id="map" style="width: 100%; height: 500px;"></div>
    </div>
    <div class="col-lg-12 margin-tb">
         <br> &nbsp;
            <div class="pull-left">
                <h2>Restaurant Management</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('resto.create') }}"> Create New Resto</a>
            </div>
        </div>
   <br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Address</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($resto as $resto_data)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $resto_data->name }}</td>
            <td>{{ $resto_data->lat }}</td>
            <td>{{ $resto_data->lng }}</td>
            <td>{{ $resto_data->address }}</td>
            <td>
                <form action="{{ route('resto.destroy',$resto_data->id) }}" method="POST">
   
                    <a class="btn btn-info btn-sm" href="{{ route('resto.show',$resto_data->id) }}">Get Direction</a>
    
                    <a class="btn btn-primary btn-sm" href="{{ route('resto.edit',$resto_data->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

   


<script>
  var map;

  function initMap() {
      
      var southeast_asia =  {lat: -0.689093, lng: 106.750281};
      map = new google.maps.Map(document.getElementById('map'), {
           center:southeast_asia,
           zoom: 5.5,
      });

      var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
              position: location,
              label: location.label
          });
      });

      var markerCluster = new MarkerClusterer(map, markers, {
          imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
      });
  }

  var locations = [
       @foreach ($resto as $resto_data)
        {lat: {{ $resto_data->lat }}, lng: {{ $resto_data->lng }}, label: '{{ $resto_data->name }}'},
       @endforeach
                 ]
        
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"> </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlfh4WuZJz51yTzzIiopDiWIA1CmntLC0&callback=initMap" sync defer></script>
  
    {!! $resto->links() !!}
      
@endsection