@extends('resto.layout')
<style>
      #right-panel {
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }

      #right-panel select, #right-panel input {
        font-size: 15px;
      }

      #right-panel select {
        width: 100%;
      }

      #right-panel i {
        font-size: 12px;
      }
      #map {
        height: 100%;
        float: left;
        width: 63%;
        height: 100%;
      }
      #right-panel {
        float: right;
        width: 34%;
        height: 100%;
      }
      .panel {
        height: 100%;
        overflow: auto;
      }
    </style>
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Direction</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('resto.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div id="map"></div>
    <div id="right-panel">
      <p>Total Distance: <span id="total"></span></p>
    </div>
    
    <script>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(initMap);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
        
        function initMap(position) {
        var currentLocation =  {lat: position.coords.latitude, lng: position.coords.longitude, label:'your location'} ;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: {lat: {{ $resto->lat }}, lng: {{ $resto->lng }}} 
        });

        var directionsService = new google.maps.DirectionsService;
        var directionsRenderer = new google.maps.DirectionsRenderer({
          draggable: true,
          map: map,
          panel: document.getElementById('right-panel')
        });

        directionsRenderer.addListener('directions_changed', function() {
          computeTotalDistance(directionsRenderer.getDirections());
        });

        displayRoute(currentLocation, directionsService, directionsRenderer);
      }

      function displayRoute(origin, service, display) {
        service.route({
          origin: origin,
          destination:   {lat: {{ $resto->lat }}, lng: {{ $resto->lng }}, label: '{{ $resto->name }}'},
          travelMode: 'DRIVING',
          avoidTolls: true
        }, function(response, status) {
          if (status === 'OK') {
            display.setDirections(response);
          } else {
            alert('Could not display directions due to: ' + status);
          }
        });
      }

      function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
          total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        document.getElementById('total').innerHTML = total + ' km';
      }
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlfh4WuZJz51yTzzIiopDiWIA1CmntLC0&callback=initMap" sync defer></script>
    </script>
    </div>
@endsection